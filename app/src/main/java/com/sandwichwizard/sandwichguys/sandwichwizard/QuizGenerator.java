package com.sandwichwizard.sandwichguys.sandwichwizard;

import android.content.Context;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;

/**
 * Class used to dynamically generate quiz questions and the ordering of the questions.
 * There is one question of each type asked in each quiz with the exception of two
 * questions asked to assess risk (adventurousness).
 */
public class QuizGenerator {
    // singleton (used so the constructor only needs to be called once per app run)
    private static QuizGenerator quizGenerator;
    // will be populated when the quiz is generated
    private String [] questions;
    private String [] responses;

    // questions of each type stored separately
    private ArrayList<String> meatQuestions = new ArrayList<String>();
    private ArrayList<String> spiceQuestions = new ArrayList<String>();
    private ArrayList<String> messyQuestions = new ArrayList<String>();
    private ArrayList<String> healthQuestions = new ArrayList<String>();
    private ArrayList<String> funQuestions = new ArrayList<String>();
    private ArrayList<String> cheeseQuestions = new ArrayList<String>();
    private ArrayList<String> riskQuestions = new ArrayList<String>();
    private ArrayList<String> fruitQuestions = new ArrayList<String>();
    private ArrayList<String> heatQuestions = new ArrayList<String>();

    // the values placed into the answers array follow this index mapping
    // 1 is low 3 is high
    private final int MEAT_INDEX = 0;
    // 1 is low 3 is high
    private final int SPICY_INDEX = 1;
    // 1 is low 3 is high
    private final int MESSY_INDEX = 2;
    // 1 is low 3 is high
    private final int HEALTHY_INDEX = 3;
    // answers are not taken into account
    private final int FUNSIE_INDEX = 4;
    // 1 is low 3 is high
    private final int CHEESE_INDEX = 5;
    // 1 is low 3 is high
    private final int RISK1_INDEX = 6;
    private final int RISK2_INDEX = 7;
    // 1 is no 2 is yes
    private final int FRUITY_INDEX = 8;
    // 1 is cold 3 is hot
    private final int HEAT_INDEX = 9;

    // number of quiz questions
    private int numQuestions = 10;
    // stores indices for question array to mix up order of presentation
    private ArrayList<Integer> questionOrder;
    // used to generate random question order
    private Random rand;

    /**
     * constructor for quiz generator, parses a text file to gather questions and responses for each topic
     * @param file indicates the address of the questions file
     * @param context app context, used to access the questions file
     */
    public QuizGenerator(int file, Context context) {
        // parses question data file line by line and sorts the questions into their respective ArrayList
        Scanner fileIn = new Scanner(context.getResources().openRawResource(file));
        String line = fileIn.nextLine().trim();
        while(!line.equals("end")) {
            switch (line) {
                case "MEATINESS":
                    line = parseTopic(meatQuestions, fileIn);
                    break;
                case "SPICINESS":
                    line = parseTopic(spiceQuestions, fileIn);
                    break;
                case "MESSINESS":
                    line = parseTopic(messyQuestions, fileIn);
                    break;
                case "HEALTHINESS":
                    line = parseTopic(healthQuestions, fileIn);
                    break;
                case "FUNSIES":
                    line = parseTopic(funQuestions, fileIn);
                    break;
                case "CHEESINESS":
                    line = parseTopic(cheeseQuestions, fileIn);
                    break;
                case "RISKINESS":
                    line = parseTopic(riskQuestions, fileIn);
                    break;
                case "FRUITINESS":
                    line = parseTopic(fruitQuestions, fileIn);
                    break;
                case "HOTNESS":
                    line = parseTopic(heatQuestions, fileIn);
                    break;
                default:
                    if(fileIn.hasNextLine())
                        line = fileIn.nextLine().trim();
                    break;
            }
        }
        // allows re-usability of quiz generator class without reconstruction
        quizGenerator = this;
        fileIn.close();
        //initialize values
        rand = new Random();
    }

    /**
     * parses a given section of the text file for a specific sandwich trait
     *
     * @param list - list that will contain questions for the given trait
     * @return next line of the file
     */
    private String parseTopic(ArrayList<String> list, Scanner in) {
        // adds all questions to the question array
        String line = in.nextLine().trim();
        while(!line.equals("end")){
            list.add(line);
            line = in.nextLine().trim();
        }
        // skips over the "end" line
        if(in.hasNextLine())
            line = in.nextLine().trim();
        return line;
    }

    /**
     * sets the question and questionorder values to a new random quiz, the result is essentially
     * a permutation of possible question orderings
     */
    public void generateQuiz() {
        // refresh quiz values for new quiz
        questions = new String[numQuestions];
        questionOrder = new ArrayList<Integer>();

        // arraylist containing all possible question indices
        ArrayList<Integer> temp = new ArrayList<Integer>();
        for(int i = 0; i < numQuestions; i ++){
            temp.add(i);
        }
        // permutes the question indices to randomize the order of topics that are addressed
        // e.g. first question regards meat, second regards cheese etc.
        while(!temp.isEmpty()) {
            if(temp.size() == 0)
                questionOrder.add(temp.remove(0));
            else
                questionOrder.add(temp.remove(Math.abs(rand.nextInt()) % temp.size()));
        }

        // gets a random question to address each sandwich trait (there are multiple for each trait)
        questions[MEAT_INDEX] = meatQuestions.get(Math.abs(rand.nextInt()) % meatQuestions.size());
        questions[SPICY_INDEX] = spiceQuestions.get(Math.abs(rand.nextInt()) % spiceQuestions.size());
        questions[MESSY_INDEX] = messyQuestions.get(Math.abs(rand.nextInt()) % messyQuestions.size());
        questions[HEALTHY_INDEX] = healthQuestions.get(Math.abs(rand.nextInt()) % healthQuestions.size());
        questions[FUNSIE_INDEX] = funQuestions.get(Math.abs(rand.nextInt()) % funQuestions.size());
        questions[CHEESE_INDEX] = cheeseQuestions.get(Math.abs(rand.nextInt()) % cheeseQuestions.size());
        questions[RISK1_INDEX] = riskQuestions.get(Math.abs(rand.nextInt()) % riskQuestions.size());

        do {
            questions[RISK2_INDEX] = riskQuestions.get(Math.abs(rand.nextInt()) % riskQuestions.size());
        } while(questions[RISK2_INDEX].equals(questions[RISK1_INDEX]));

        questions[FRUITY_INDEX] = fruitQuestions.get(Math.abs(rand.nextInt()) % fruitQuestions.size());
        questions[HEAT_INDEX] = heatQuestions.get(Math.abs(rand.nextInt()) % heatQuestions.size());
    }

    /**
     * returns the single instance of the quizgenerator class
     * @return QuizGenerator
     */
    public static QuizGenerator getSingleton() {
        return quizGenerator;
    }

    /**
     * returns the permuted question order, called after generateQuiz()
     * @return ArrayList<Integer> a permuted ordering of the question indices
     */
    public ArrayList<Integer> getQuestionOrder() { return questionOrder; }

    /**
     * returns the array of questions for the quiz, called after generateQuiz()
     * @return String[] array containing all questions that will be asked on the quiz
     */
    public String[] getQuestions(){ return questions; }

    /**
     * returns the number of questions in the quiz
     * @return numQuestions
     */
    public int getNumQuestions() { return numQuestions; }
}
