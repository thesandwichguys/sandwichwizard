package com.sandwichwizard.sandwichguys.sandwichwizard;

import java.util.LinkedList;
import java.util.List;

/*
 * Represents a sandwich, and contains relevant information, such as a unique
 * sandwich id (also indicates sandwich qualities), the sandwich name, and a
 * URL of a recipe for the sandwich
 * 
 * @author Kyle Ferris, Rose Walton
 */
public class Sandwich {
    /*
     * id number of sandwich
     *
     * 1st number = meatiness (1 = vegitarian 2 = omnivore 3 = carnivore)
     * 2nd number = spiciness (1 = least spicy 3 = most spicy)
     * 3rd number = messiness (1 = clean as a baby's bottom 3 = Philly Cheesesteak level shit)
     * 4th number = healthiness (1 = unhealthy 2 = healthy)
     * 5th number = cheesiness (1 = least 3 = most)
     * 6th number = riskiness (1 = ham and cheese type stuff 3 = mango pulled pork business)
     * 7th number = fruityness (1 = no, 2 = yes)
     * 8th number = hotness (1 = cold 3 = hot)
     */
    private String id;
    //name of the sandwich
    private String name;
    //website of where the sandwich recipe can be found
    private String recipeURL;

    private int picture = 0;

    private List<Ingredient> ingredients;

    /*
     * Constructs a sandwich object and assigns the specified sandwich ID, name,
     * and recipe location.
     *
     * @String id -unique number to identify the sandwich
     * @String name -name of the sandwich
     * @String recipeURL -url of a recipe for the sandwich
     */
    public Sandwich(String id, String name, String recipeURL) {
        this.setId(id);
        this.setName(name);
        this.setRecipeURL(recipeURL);
        this.ingredients = new LinkedList<Ingredient>();
        this.setPicture();
        // Gets a new array of ingredients, with all elements set to false
    }

    /*
    * Returns the sandwich picture as an integer (R.drawable.[picture])
    * @return int picture
    */
    public int getPicture() {
        return picture;
    }

    private void setPicture() {
        picture = R.drawable.baked_bean;
        switch (name) {
            case "Baked Bean":
                picture = R.drawable.baked_bean;
                break;
            case "BLT":
                picture = R.drawable.blt;
                break;
            case "Bourbon Mango Pulled Pork":
                picture = R.drawable.bourbon_mango_pulled_pork;
                break;
            case "Bourbon Mango Pulled Summer Squash":
                picture = R.drawable.bourbon_mango_pulled_summer_squash;
                break;
            case "Brie and Apple Grilled Cheese":
                picture = R.drawable.brie_and_apple_grilled_cheese;
                break;
            case "Chicken Pot Pie Empanada":
                picture = R.drawable.chicken_pot_pie_enpenada;
                break;
            case "Corn Dog":
                picture = R.drawable.corn_dog;
                break;
            case "Drunken Grilled Cheese":
                picture = R.drawable.drunken_grilled_cheese;
                break;
            case "Egg and Broccolini":
                picture = R.drawable.egg_and_broccolini;
                break;
            case "Grilled Cashew Butter and Blueberry":
                picture = R.drawable.grilled_cashew_butter_and_blueberry;
                break;
            case "Inside Out Grilled Ham and Cheese":
                picture = R.drawable.inside_out_grilled_ham_and_cheese;
                break;
            case "Italian Sloppy Joe":
                picture = R.drawable.italian_sloppy_joe;
                break;
            case "Leftover Dog Pile":
                picture = R.drawable.leftover_dog_pile;
                break;
            case "Meatball Sandwich":
                picture = R.drawable.meatball;
                break;
            case "Mediterranean Chicken Gyro":
                picture = R.drawable.mediterranean_chicken_gyro;
                break;
            case "Peanut Butter and Fruit":
                picture = R.drawable.peanut_butter_and_fruit;
                break;
            case "Pickled Tuna Salad":
                picture = R.drawable.pickled_tuna_salad;
                break;
            case "Portabella Mushroom Cheesesteak":
                picture = R.drawable.portabella_mushroom_cheesesteak;
                break;
            case "Puerto Sagua Cuban Sandwich":
                picture = R.drawable.puerto_sagua_cuban_sandwich;
                break;
            case "Sloppy Joe":
                picture = R.drawable.sloppy_joe;
                break;
            case "Stromboli":
                picture = R.drawable.stromboli;
                break;
            case "Sweet and Spicy Caramelized Onion BBQ":
                picture = R.drawable.sweet_and_spicy_caramelized_onion_bbq;
                break;
            case "Sweet and Spicy Turkey":
                picture = R.drawable.sweet_and_spicy_turkey;
                break;
            case "Tofu Banh Mi":
                picture = R.drawable.tofu_banh_mi;
                break;
            case "Turkey Avocado":
                picture = R.drawable.turkey_avocado;
                break;
            case "Turkey French Dip":
                picture = R.drawable.turkey_french_dip;
                break;
            case "Vegetable Panini":
                picture = R.drawable.vegetable_panini;
                break;
            case "Vegetarian Meatball Parm":
                picture = R.drawable.vegetarian_meatball_parm;
                break;
            case "Arch Deluxe":
                picture = R.drawable.arch_deluxe;
                break;
            case "Bacon Egg and Cheese":
                picture = R.drawable.bacon_egg_and_cheese;
                break;
            case "Roast Beef on Weck":
                picture = R.drawable.roast_beef_on_weck;
                break;
            case "Cheese Dream":
                picture = R.drawable.cheese_dream;
                break;
            case "Chicken Sandwich":
                picture = R.drawable.chicken_sandwich;
                break;
            case "Fluffernutter":
                picture = R.drawable.fluffernutter;
                break;
            case "Fool's Gold Loaf":
                picture = R.drawable.fools_gold_loaf;
                break;
            case "Fried Brain Sandwich":
                picture = R.drawable.fried_brain_sandwich;
                break;
            case "Juicy Lucy":
                picture = R.drawable.juicy_lucy;
                break;
            case "Monte Cristo Sandwich":
                picture = R.drawable.monte_cristo_sandwich;
                break;
            case "Lobster Roll":
                picture = R.drawable.lobster_roll;
                break;
            case "Fat Darrell Sandwich":
                picture = R.drawable.fat_darrell_sandwich;
                break;
            default:
                picture = R.drawable.yuck;
                break;
        }
    }

    /*
     * Returns the unique sandwich ID value.
     * @return String id
     */
    public String getId() {
        return id;
    }

    /*
     * Sets the unique sandwich ID value.
     * @param String id
     */
    public void setId(String id) {
        this.id = id;
    }

    /*
     * Returns the sandwich name.
     * @return String name
    */
    public String getName() {
        return name;
    }

    /*
     * Sets the sandwich name.
     * @param String name
     */
    public void setName(String name) {
        this.name = name;
    }

    /*
     * Returns the URL location of the sandwich recipe.
     * @return String recipeURL
     */
    public String getRecipeURL() {
        return recipeURL;
    }

    /*
     * Sets the URL location of the sandwich recipe.
     * @param String recipeURL
     */
    public void setRecipeURL(String recipeURL) {
        this.recipeURL = recipeURL;
    }

    @Override
    public String toString() {
        return name + "\n" + recipeURL;
    }

    public void addIngredient(Ingredient newIngredient) {
        ingredients.add(newIngredient);
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    @Override
    public boolean equals(Object other) {
        Sandwich otherSandwich;
        if (other instanceof Sandwich)
            otherSandwich = (Sandwich) other;
        else
            return false;
        if (this.getName().equals(otherSandwich.getName()))
            return true;
        else
            return false;
    }

    public boolean hasIngredient(Ingredient ingredient) {

        for (Ingredient ing : ingredients)
            if (ing.compareIngredient(ingredient))
                return true;

        return false;

    }


}
