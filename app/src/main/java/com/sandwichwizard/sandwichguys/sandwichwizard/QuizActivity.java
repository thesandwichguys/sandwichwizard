package com.sandwichwizard.sandwichguys.sandwichwizard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Kyle on 3/12/2015.
 *
 * Manages all of the quiz activity setup and behavior, using the quiz generator
 * class to produce obtain the quiz questions and ordering. Once the quiz is complete,
 * this class routes the user to a page with a sandwich result.
 */
public class QuizActivity extends FragmentActivity {
    // gets the static instances of the sandwich menu and quiz generator
    SandwichMenu sandwichMenu = SandwichMenu.getSingleton();
    QuizGenerator quizGenerator = QuizGenerator.getSingleton();

    // the values placed into the answers array follow this index mapping
    // 1 is low 3 is high
    private final int MEAT_INDEX = 0;
    // 1 is low 3 is high
    private final int SPICY_INDEX = 1;
    // 1 is low 3 is high
    private final int MESSY_INDEX = 2;
    // 1 is low 3 is high
    private final int HEALTHY_INDEX = 3;
    private final int FUNSIE_INDEX = 4;
    // 1 is low 3 is high
    private final int CHEESE_INDEX = 5;
    // 1 is low 3 is high
    private final int RISK1_INDEX = 6;
    private final int RISK2_INDEX = 7;
    // 1 is no 2 is yes
    private final int FRUITY_INDEX = 8;
    // 1 is cold 3 is hot
    private final int HEAT_INDEX = 9;

    // will contain all of the questions for the quiz
    String[] questions;
    // will contain quiz responses
    int[] answers;
    // will contain the ordering of the questions
    ArrayList<Integer> questionOrder;

    // address of progress bar for modification
    ProgressBar progressBar;
    // address of question area for modification
    TextView questionArea;
    // initializes button view items for responses
    Button response1;
    Button response2;
    Button response3;
    // Button response4;

    // index of current question
    int currQuestion = 0;

    /**
     * Initializes the buttons, generates the quiz with the quiz generator, initalizes
     * class arrays, and initializes the progress bar
     *
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // sets up activity view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_screen);

        // obtains pointers to necessary views
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        questionArea = (TextView) findViewById(R.id.textView);
        response1 = (Button) findViewById(R.id.answer_1);
        response1.setVisibility(View.INVISIBLE);
        response1 = (Button) findViewById(R.id.answer_2);
        response2 = (Button) findViewById(R.id.answer_3);
        response3 = (Button) findViewById(R.id.answer_4);

        // set buttons to respond to a button press
        response1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPress(1);
            }
        });
        response2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPress(2);
            }
        });
        response3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPress(3);
            }
        });

        // initialize values
        answers = new int[quizGenerator.getNumQuestions()];
        quizGenerator.generateQuiz();
        questions = quizGenerator.getQuestions();
        questionOrder = quizGenerator.getQuestionOrder();
        progressBar.setMax(answers.length);
        progressBar.setProgress(0);
    }

    /**
     * begins quiz by calling the present question method, all subsequent calls
     * to present question will be the result of button presses
     */
    @Override
    protected void onStart() {
        super.onStart();
        presentQuestion(questionOrder.get(currQuestion));
    }

    /**
     * sets up the options menu appropriately
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * sets up options menu behavior
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * presents the question from the question array at the given index
     *
     * @param index - the index in the question array correlating to the question to ask
     */
    private void presentQuestion(int index) {
        // contains the question string at index 0 and answer string at index 1
        String[] qanda = questions[index].split(";");
        // sets question text
        questionArea.setText(qanda[0]);
        // splits up possible answers into individual string values
        String[] splitResponses = qanda[1].split(",");
        response1.setText(splitResponses[0].trim());
        response2.setText(splitResponses[1].trim());
        // some questions have 3 responses, some have two and only require two visible buttons
        response3.setVisibility(View.INVISIBLE);
        if (splitResponses.length > 2) {
            response3.setText(splitResponses[2]);
            response3.setVisibility(View.VISIBLE);
        }
    }

    /**
     * handles button presses, which correspond to responses being input into the quiz
     *
     * @param buttonNum - indicates which button is pressed, ranges from 1 to 3
     */
    private void buttonPress(int buttonNum) {
        // saves response into answers array
        answers[questionOrder.get(currQuestion)] = buttonNum;
        // advances activity to the next question
        currQuestion++;
        // advances progress bar
        progressBar.setProgress(currQuestion);
        // presents next question if there is one, otherwise, the results are calculated
        if (currQuestion < questions.length)
            presentQuestion(questionOrder.get(currQuestion));
        else
            getResults();
    }

    /**
     * uses the values in the answers array to calculate a sandwich ID, the best match
     * between this ID and those on the menu is found, then a page displaying that
     * sandwich is brought up
     */
    private void getResults() {
        // sets up sandwich ID
        String sandwichID = "";
        sandwichID += answers[MEAT_INDEX] + "";
        sandwichID += answers[SPICY_INDEX] + "";
        sandwichID += answers[MESSY_INDEX] + "";
        sandwichID += answers[HEALTHY_INDEX];
        sandwichID += answers[CHEESE_INDEX] + "";
        sandwichID += (answers[RISK1_INDEX] + answers[RISK2_INDEX] - 1) + "";
        sandwichID += answers[FRUITY_INDEX] + "";
        sandwichID += answers[HEAT_INDEX] + "";

        // finds best sandwich match on the menu
        Sandwich result = sandwichMenu.getSandwich(sandwichID);

        // opens up new activity to display the sandwich
        Intent intent = new Intent(this, Display.class);
        // passes in sandwich ID result to the display activity
        if(result != null)
            intent.putExtra("id", result.getId());
        else
            intent.putExtra("id", "0");
        startActivity(intent);
    }
}