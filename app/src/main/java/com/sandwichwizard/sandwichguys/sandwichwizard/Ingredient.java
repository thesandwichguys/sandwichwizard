package com.sandwichwizard.sandwichguys.sandwichwizard;

/**
 * Created by andrewdaniel on 3/15/15.
 */
public class Ingredient {
    private SandwichIngredients.IngredientTypes type;
    private String ingredientName;

    /**
     * The public constructor
     */
    public Ingredient() {
    }

    /**
     * The public constructor which takes a string as the ingredient name
     *
     * @param ingName
     */
    public Ingredient(String ingName) {
        ingredientName = ingName.trim();
    }

    /**
     * Gets the name of this sandwich
     *
     * @return the name of this sandwich
     */
    public String getName() {
        return ingredientName;
    }

    /**
     * Sets the name of this sandwich. This is displayed directly in the CustomizationActivity in
     * the ingredient listview
     *
     * @param newName the name to set
     */
    public void setName(String newName) {
        this.ingredientName = newName.trim();
    }

    /**
     * Sets the type of ingredient that this is (cheese, meat, condiment etc)
     *
     * @param newType the type of ingredient this is
     */
    public void setType(SandwichIngredients.IngredientTypes newType) {
        this.type = newType;
    }

    /**
     * Compares two ingredients to see whether they are identical. For example, this function would
     * return true if the argument was "butter", and this ingredient was "butter" but would return
     * false if this ingredient was "garlic butter". If you want this functionality, use
     * ingredientsAreSimilar()
     *
     * @param ingredient the ingredient to compare
     * @return a boolean for whether these ingredients are identical
     */
    public boolean compareIngredient(Ingredient ingredient) {
        return this.ingredientName.trim().equals(ingredient.getName().trim());
    }

    /**
     * Used to get ingredients that might be similar. In other words, if you pass the ingredient
     * "cheese", this function would return true for "pepperjack cheese", but false for "pepperjack"
     *
     * @param ingredient the ingredient to compare
     * @return a boolean for whether these ingredients are similar
     */
    public boolean ingredientsAreSimilar(Ingredient ingredient) {
        return ingredientName.contains(ingredient.getName())
                || ingredient.getName().contains(ingredientName);
    }
}
