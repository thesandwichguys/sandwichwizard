package com.sandwichwizard.sandwichguys.sandwichwizard;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

/**
 * Activity class for viewing favorited sandwiches
 *
 * @author Ogden Greene
 */
public class Favorites extends ListActivity {
    SandwichMenu sandwichMenu;
    // array of string names of favorite sandwiches
    String[] faves;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        sandwichMenu = SandwichMenu.getSingleton();
        faves = PreferencesHelper.getFavorites(this);
        setListAdapter(new myAdapter());
        // if no favorites, show text
        if (faves.length == 0) {
            findViewById(R.id.favoritesTitle).setVisibility(View.INVISIBLE);
            findViewById(R.id.noFavorites).setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // reset list in case something was unfavorited from the activity
        faves = PreferencesHelper.getFavorites(this);
        setListAdapter(new myAdapter());
        if (faves.length == 0) {
            findViewById(R.id.favoritesTitle).setVisibility(View.INVISIBLE);
            findViewById(R.id.noFavorites).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onListItemClick(ListView parent, View v, int i, long id) {
        Intent intent = new Intent(this, Display.class);
        String sandId = sandwichMenu.getSandwichByName(faves[i]).getId();
        intent.putExtra("id", sandId);
        startActivity(intent);
    }

    /**
     * Extension of ArrayAdapter to include the row layout
     */
    class myAdapter extends ArrayAdapter<String> {
        myAdapter() {
            super(Favorites.this, R.layout.row, R.id.name, faves);
        }

        @Override
        public View getView(int i, View v, ViewGroup parent) {
            View row = super.getView(i, v, parent);
            // set the sandwich picture
            ImageView picture = (ImageView) row.findViewById(R.id.picture);
            Sandwich sandwich = sandwichMenu.getSandwichByName(faves[i]);
            picture.setImageResource(sandwich.getPicture());
            return row;
        }

    }

}
