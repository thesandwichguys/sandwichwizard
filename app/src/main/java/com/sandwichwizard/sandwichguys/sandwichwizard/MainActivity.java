package com.sandwichwizard.sandwichguys.sandwichwizard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/**
 * Main activity class! Navigational elements and such
 *
 * @author Ogden Greene, Andrew Daniel, Kyle Ferris, Luke Salamone
 */
public class MainActivity extends ActionBarActivity {


    /**
     * Navigates to the ShakeActivity
     *
     * @param view button view
     */
    public void goToRandom(View view) {
        Intent i = new Intent(this, ShakerActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Deserializes the sandwich menu only one time (not needed more than once)
        SandwichMenu.deserializeSandwichMenu(this);
        QuizGenerator qg = new QuizGenerator(R.raw.quiz_questions, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // navigate to the Settings activity
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, Settings.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Navigates to the QuizActivity
     *
     * @param view button view
     */
    public void startQuiz(View view) {
        Intent intent = new Intent(this, QuizActivity.class);
        startActivity(intent);
    }

    /**
     * Navigates to the CustomizationActivity
     *
     * @param view button view
     */
    public void startCustomization(View view) {

        Intent intent = new Intent(this, CustomizationActivity.class);
        startActivity(intent);
    }

    /**
     * Navigates to the Favorites activity
     *
     * @param view button view
     */
    public void startFavorites(View view) {
        Intent intent = new Intent(this, Favorites.class);
        startActivity(intent);
    }
}
