package com.sandwichwizard.sandwichguys.sandwichwizard;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/*
 * Menu object that reads in sandwich data from a file and places the 
 * information into a list.
 * 
 * @author Kyle Ferris, Andrew Daniel, Ogden Greene, Luke Salamone
 */
public class SandwichMenu {
    // The singleton representing the sandwich menu
    private static SandwichMenu menu;
    //stores all the sandwich objects
    private ArrayList<Sandwich> sandwiches;

    private ArrayList<Ingredient> ingredients;
    private Random rangen;

    /**
     * Reads in sandwich data from a file of the format
     * Sandwich ID#
     * Sandwich Name
     * Sandwich Recipe URL
     * <p/>
     * usage: SandwichMenu(R.raw.sandwich_ingredients, this)
     */
    private SandwichMenu(Context context) {
        sandwiches = new ArrayList<Sandwich>();
        ingredients = new ArrayList<Ingredient>();
        rangen = new Random();

        Scanner fileIn = new Scanner(context.getResources().openRawResource(R.raw.sandwich_ingredients));
        boolean moreSandwiches = true;

        while (moreSandwiches) {
            // Add a new sandwich to the database
            Sandwich sandwich = parseNextSandwich(fileIn);
            if (!PreferencesHelper.containsSandwich(sandwich.getName(), context, PreferencesHelper.YUCK))
                sandwiches.add(sandwich);

            if (!fileIn.hasNextLine())
                moreSandwiches = false;
        }
        fileIn.close();

        // Go through each sandwich and add its ingredients to the ingredient list
        for (Sandwich sandwich : sandwiches) {
            // Go through each ingredient in the sandwich
            for (Ingredient ingredient : sandwich.getIngredients()) {
                boolean isDuplicate = false;
                // Check to see if the name is duplicated in the list of ingredients
                for (Ingredient currIngredient : ingredients)
                    if (ingredient.getName().equals(currIngredient.getName())) {
                        isDuplicate = true;
                    }
                // If the ingredient is not already in the list of ingredients, add it
                if (!isDuplicate)
                    ingredients.add(ingredient);
            }
        }
    }

    /**
     * Creates the sandwich menu. Returns the number of sandwiches that were deserialized
     *
     * @param context the android context in which this is used. Used to get at the raw resources
     * @return the number of deserialized sandwiches
     */
    public static int deserializeSandwichMenu(Context context) {
        menu = new SandwichMenu(context);
        return menu.getSandwiches().size();
    }

    public static SandwichMenu getSingleton() {
        return menu;
    }

    /**
     * This is a helper function which parses the sandwich from the sandwiches file, adding ingredients and such
     *
     * @param fileIn the file scanner
     * @return the completely instantiated sandwich object
     */
    private Sandwich parseNextSandwich(Scanner fileIn) {
        // The integer in the sandwich file
        String id = fileIn.nextLine().trim();
        // The name of the sandwich
        String name = fileIn.nextLine().trim();
        // Where the sandwich comes from
        String url = fileIn.nextLine().trim();
        // Make the sandwich
        Sandwich sandwich = new Sandwich(id, name, url);

        boolean done = false;
        while (!done) {
            // Create the new ingredient and set the name and type
            Ingredient newIngredient = new Ingredient();
            String ingredientName = fileIn.nextLine().trim();

            // Checks for the end of this sandwich
            if (!ingredientName.equals("end"))
                newIngredient.setName(ingredientName);
            else
                break;

            // Get the type of the ingredient
            SandwichIngredients.IngredientTypes type = SandwichIngredients.getIngredientType(newIngredient.getName());
            newIngredient.setType(type);
            sandwich.addIngredient(newIngredient);
            // Checks end of file
            done = ingredientName.equals("end") || !fileIn.hasNextLine();

        }
        return sandwich;
    }

    /**
     * This method takes a unique id generated when taking the quiz. Returns a sandwich which matches
     * that id
     *
     * @param id the id to match
     * @return the sandwich containing the id
     */
    public Sandwich getSandwich(String id) {
        int maxMatchCount = 0;
        int sandwichIndex = -1;

        for (int i = 0; i < sandwiches.size(); i++) {

            String tempId = sandwiches.get(i).getId();
            int matchCount = 0;
            for (int j = 0; j < id.length(); j++) {
                if (id.charAt(j) == tempId.charAt(j))
                    matchCount++;
                else if (j == 0 && id.charAt(j) == '1') {
                    matchCount = -100;
                    j = id.length();
                }
            }
            if (matchCount > maxMatchCount) {
                maxMatchCount = matchCount;
                if (maxMatchCount >= 3)
                    sandwichIndex = i;
            }
        }
        if (sandwichIndex == -1)
            return null;

        return sandwiches.get(sandwichIndex);
    }

    public List<Sandwich> getSandwiches() {
        return sandwiches;
    }

    /**
     * Returns a totally random sandwich, dude
     *
     * @return Sandwich randomly
     */
    public Sandwich getSandwich() {
        return sandwiches.get(rangen.nextInt(sandwiches.size()));
    }

    /**
     * Gets sandwich by its exact ID
     * Returns null if a match doesn't exist
     *
     * @param ID string ID of sandwich
     * @return matching sandwich (or null if none found)
     */
    public Sandwich getSandwichByID(String ID) {
        for (int i = 0; i < sandwiches.size(); i++) {
            Sandwich curr = sandwiches.get(i);
            if (curr.getId().equals(ID))
                return curr;
        }
        return null;
    }

    /**
     * Gets sandwich by its string name
     * Returns null if a match doesn't exist
     *
     * @param name string name of sandwich
     * @return matching sandwich (or null if none found)
     */
    public Sandwich getSandwichByName(String name) {
        for (int i = 0; i < sandwiches.size(); i++) {
            Sandwich curr = sandwiches.get(i);
            if (curr.getName().equals(name))
                return curr;
        }
        return null;
    }

    public List<Ingredient> getAllIngredients() {
        return ingredients;
    }

    /**
     * This method returns a list of sandwiches in the database that contain a number of ingredients
     * in the list. the number of ingredients that a sandwich must have to be included in the return
     * value is specified by ingredientCount.
     *
     * @param ingredientList  the list of ingredients to match
     * @param ingredientCount how many ingredients a sandwich must have to be included
     * @return the list of sandwiches containing the ingredients specified
     */
    public List<Sandwich> getAllSandwichesContainingIngredients(List<Ingredient> ingredientList, int ingredientCount) {

        List<Sandwich> compiledList = new ArrayList<Sandwich>();

        // Iterate through all sandwiches for a match
        for (Sandwich wich : sandwiches) {

            int count = 0;
            // Iterate through all ingredients in the ingredient list to find best fit sandwiches
            for (Ingredient ingredient : ingredientList) {

                // If this sandwich has this ingredient, add it to the count
                if (wich.hasIngredient(ingredient))
                    count++;
            }

            // If the count exceeds the requested ingredient count, add this sandwich to the list
            if (count >= ingredientCount)
                compiledList.add(wich);
        }
        return compiledList;
    }
}
