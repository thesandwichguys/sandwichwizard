package com.sandwichwizard.sandwichguys.sandwichwizard;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Settings activity with buttons to reset different preferences
 *
 * @author Ogden Greene
 */
public class Settings extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

    }

    /**
     * Resets the yucks when button pressed
     *
     * @param view button view
     */
    public void yucks(View view) {
        PreferencesHelper.reset(this, PreferencesHelper.YUCK);
        SandwichMenu.deserializeSandwichMenu(this);
        CharSequence msg = getResources().getString(R.string.yucks_cleared);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Resets the favorites when button pressed
     *
     * @param view button view
     */
    public void faves(View view) {
        PreferencesHelper.reset(this, PreferencesHelper.FAVORITE);
        CharSequence msg = getResources().getString(R.string.faves_cleared);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Resets the favorites, yucks, and "fire-time" attribute when button pressed
     *
     * @param view button view
     */
    public void reset(View view) {
        PreferencesHelper.resetAll(this);
        SandwichMenu.deserializeSandwichMenu(this);
        CharSequence msg = getResources().getString(R.string.reset);
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}
