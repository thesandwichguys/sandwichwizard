package com.sandwichwizard.sandwichguys.sandwichwizard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * This class handles the behavior of the sandwich customization. A sandwich can be search for by
 * its ingredients, and displayed in a list view.
 *
 * @author Andrew Daniel, Ogden Greene
 */
public class CustomizationActivity extends Activity {

    // List view holding the ingredients
    private ListView customizationListView = null;
    // Listview holding the sandwiches
    private ListView sandwichListView = null;
    // The buttons in this activity
    private Button filterButton = null;
    private Button submitButton = null;
    // The list of sandwiches
    private List<Sandwich> sandwiches = null;
    // The database of sandwiches
    private SandwichMenu sandwichMenu;
    // Custom adapters to show ingredients and sandwiches
    private SandwichAdapter sandwichAdapter;
    private FilterOptionAdapter filterOptionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customization);
        // Set up the list views
        customizationListView = (ListView) findViewById(R.id.customizationListView);
        sandwichListView = (ListView) findViewById(R.id.sandwichListView);
        // Get the references to the buttons
        filterButton = (Button) findViewById(R.id.cust_filterButton);
        submitButton = (Button) findViewById(R.id.cust_submitButton);
        // Sets up the reference to the sandwich database and deserializes it if necessary
        if ((sandwichMenu = SandwichMenu.getSingleton()) == null) {
            SandwichMenu.deserializeSandwichMenu(this);
            sandwichMenu = SandwichMenu.getSingleton();
        }
        sandwiches = sandwichMenu.getSandwiches();
        // Create and set the adapter for the sandwich list
        sandwichAdapter = new SandwichAdapter();
        sandwichListView.setAdapter(sandwichAdapter);
        // Get all the ingredients
        List<Ingredient> ingredients = SandwichMenu.getSingleton().getAllIngredients();
        // Setup the adapter for the ingredients
        filterOptionAdapter = new FilterOptionAdapter(this, ingredients);
        customizationListView.setAdapter(filterOptionAdapter);
        // Set initial visibilities
        sandwichListView.setVisibility(View.GONE);
        filterButton.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onCreate(null);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customization, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This callback method handles what to do when the submit button on the activity is pressed.
     *
     * @param view
     */
    public void onSubmitButtonSelected(View view) {

        // Hide the ingredient list and the submit button
        customizationListView.setVisibility(View.GONE);
        submitButton.setVisibility(View.GONE);

        // Show the sandwich list and the filters button
        sandwichListView.setVisibility(View.VISIBLE);
        filterButton.setVisibility(View.VISIBLE);

        // Get the list of ingredients that were checked
        List<Ingredient> checkedIngs = filterOptionAdapter.getCheckedIngredients();

        // Get the list of sandwiches containing the checked ingredients
        sandwiches = SandwichMenu.getSingleton().getAllSandwichesContainingIngredients(checkedIngs, 1);

        // Reset the sandwich adapter
        sandwichAdapter = new SandwichAdapter();
        sandwichListView.setAdapter(sandwichAdapter);
    }

    /**
     * This callback function handles the actions to perform when the filters button is selected
     *
     * @param view
     */
    public void onFilterButtonSelected(View view) {

        // Show the ingredient list and the submit button
        customizationListView.setVisibility(View.VISIBLE);
        submitButton.setVisibility(View.VISIBLE);

        // Hide the sandwich list and the filters button
        sandwichListView.setVisibility(View.GONE);
        filterButton.setVisibility(View.GONE);
    }

    /**
     * This class extends an ArrayAdapter of sandwiches. This adapter shows both the name of a
     * sandwich and its corresponding picture in a listview layout.
     */
    private class SandwichAdapter extends ArrayAdapter<Sandwich> {

        /**
         * The public constructor
         */
        public SandwichAdapter() {
            super(CustomizationActivity.this, R.layout.filter_list_item_layout,
                    R.id.cust_sandwichListName, sandwiches);
        }

        /**
         * Gets the current row by position
         *
         * @param position    the row index to get
         * @param convertView the view context
         * @param parent      the parent of this view
         * @return the row at specified position
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = super.getView(position, convertView, parent);
            if (position < sandwiches.size()) {
                Sandwich sand = sandwiches.get(position);
                final String sandName = sand.getName();
                TextView textView = (TextView) row.findViewById(R.id.cust_sandwichListName);
                textView.setText(sandName.toCharArray(), 0, sandName.length());
                ImageView imageView = (ImageView) row.findViewById(R.id.cust_sandwichImage);
                imageView.setImageResource(sand.getPicture());
                row.setOnClickListener(new View.OnClickListener() {

                    /**
                     * This method handles what to do when a given sandwich is clicked
                     * @param v
                     */
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), Display.class);
                        String sandId = sandwichMenu.getSandwichByName(sandName).getId();
                        intent.putExtra("id", sandId);
                        // Show the sandwich summary screen
                        startActivity(intent);
                    }
                });
            }
            return row;
        }
    }

    /**
     * This class extends an ArrayAdapter holding ingredients. Shows both a checkbox and the name
     * of the ingredient. Furthermore, it handles what to do when a checkbox is selected, and ensures
     * that the list view of checkboxes is updated.
     */
    private class FilterOptionAdapter extends ArrayAdapter<Ingredient> {

        // The list of ingredients to display
        private List<Ingredient> ingredientList;
        private LayoutInflater mInflater;
        // Holds which ingredients where checked with each use of the list view. This is needed so
        // that as the listview is scrolled, multiple check boxes are not checked (a glitch with how
        // these listviews work)
        private ArrayList<Boolean> positionArray;

        /**
         * The public constructor
         *
         * @param context the context in which this adapter works
         * @param myList  the list of ingredients to display
         */
        public FilterOptionAdapter(Context context, List<Ingredient> myList) {
            super(context, NO_SELECTION);
            ingredientList = myList;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // Sets the initial values for the boolean array
            positionArray = new ArrayList<Boolean>(myList.size());
            for (int i = 0; i < myList.size(); i++)
                positionArray.add(false);
        }

        /**
         * Gets the size of the list of ingredients being shown
         *
         * @return the length of the list of ingredients
         */
        public int getCount() {
            return ingredientList.size();
        }

        /**
         * Gets which ingredients in the list view were checked.
         *
         * @return the list of ingredients which are checked
         */
        public List<Ingredient> getCheckedIngredients() {

            List<Ingredient> ingsToRet = new ArrayList<Ingredient>();

            for (int i = 0; i < positionArray.size(); i++)
                if (positionArray.get(i))
                    ingsToRet.add(ingredientList.get(i));

            return ingsToRet;
        }

        /**
         * Handles the rendering of the list view of checkboxes and ingredients. This method is a
         * solution to the issue of multiple check boxes being selected when only one was actually
         * selected.
         *
         * @param position    the row index to get
         * @param convertView the view layout holding the actual row
         * @param parent      the parent of the view to get
         * @return the row (checkbox + ingredient name) to get
         */
        public View getView(final int position, View convertView, ViewGroup parent) {

            View row = convertView;
            Holder holder = null;

            // Create a new Checkbox and tag if the row is null
            if (row == null) {
                row = mInflater.inflate(R.layout.filter_checkbox_layout, null);
                holder = new Holder();
                holder.ckbox = (CheckBox) row.findViewById(R.id.cust_checkBox);
                row.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
                holder.ckbox.setOnCheckedChangeListener(null);
            }

            // Set the checkbox to be focused and checked depending on the position of this row with
            // the boolean array
            holder.ckbox.setFocusable(false);
            holder.ckbox.setChecked(positionArray.get(position));
            // Get the name of the ingredient and set it as the text here
            holder.ckbox.setText(ingredientList.get(position).getName());
            holder.ckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /**
                 * When a checkbox is selected, set the corresponding position in the boolean array
                 * @param buttonView
                 * @param isChecked whether this checkbox is becoming checked or unchecked
                 */
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        System.out.println(position + "--- :)");
                        positionArray.set(position, true);
                    } else
                        positionArray.set(position, false);
                }
            });

            return row;
        }
    }

    // Convenience class for holding the checkbox to be retrieved by tag in getView()
    static class Holder {
        CheckBox ckbox;
    }
}

