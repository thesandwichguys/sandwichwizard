package com.sandwichwizard.sandwichguys.sandwichwizard;

/**
 * Basic skeleton interface for shake listener
 *
 * @author Ogden Greene
 */
public interface OnShakeListener {
    public void onShake();
}