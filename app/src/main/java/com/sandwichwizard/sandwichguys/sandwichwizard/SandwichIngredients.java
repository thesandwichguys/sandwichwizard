package com.sandwichwizard.sandwichguys.sandwichwizard;

/**
 * This class contains some helper methods for Ingredients. Mainly to return true if an ingredient
 * is of a given type by the name of it.
 *
 * Created by andrewdaniel on 3/6/15.
 */
public class SandwichIngredients {


    /**
     * The types of ingredient in a sandwich
     */
    public enum IngredientTypes {
        MEATS,
        CHEESES,
        VEGETABLES,
        CONDIMENT,
        MISCELLANEOUS
    }

    /**
     * Gets a name of an ingredient as a string then returns an ingredient type.
     *
     * @param name the name of the ingredient type to get
     * @return the ingredient type
     */
    public static IngredientTypes getIngredientType(String name) {

        name = name.toLowerCase();
        //TODO fix this class some more
        if (isCheese(name))
            return IngredientTypes.CHEESES;
        else if (isMeat(name))
            return IngredientTypes.MEATS;
        else if (isVegetable(name))
            return IngredientTypes.VEGETABLES;
        else if (isCondiment(name))
            return IngredientTypes.CONDIMENT;
        else
            return IngredientTypes.MISCELLANEOUS;
    }

    /**
     * Takes a string and returns whether the string defines a cheese
     *
     * @param name
     * @return true if the ingredient is a cheese
     */
    private static boolean isCheese(String name) {
        if (name.contains("cheese")
                || name.contains("parmesan")
                || name.contains("cheddar")
                || name.contains("pepperjack")
                || name.contains("munster")
                || name.contains("colby")
                || name.contains("swiss")
                || name.contains("american"))
            return true;
        return false;
    }

    /**
     * Whether the ingredient is a meat
     *
     * @param name the name of the ingredient
     * @return true if the ingredient is a meat
     */
    private static boolean isMeat(String name) {
        if (name.contains("pork")
                || name.contains("chicken")
                || name.contains("ham")
                || name.contains("turkey")
                || name.contains("salami")
                || name.contains("pepperoni")
                || name.contains("beef"))
            return true;
        else
            return false;
    }

    /**
     * Returns whether an ingredient is a vegetable
     *
     * @param name the name of the ingredient
     * @return true if the ingredient is a vegetable
     */
    private static boolean isVegetable(String name) {

        if (name.contains("lettuce")
                || name.contains("pepper")
                || name.contains("tomato")
                || name.contains("onion")
                || name.contains("pickle"))
            return true;
        else
            return false;
    }

    /**
     * Returns whether an ingredient is a condiment
     *
     * @param name the name of the ingredient
     * @return true if the ignredient is a condiment
     */
    private static boolean isCondiment(String name) {
        if (name.contains("ketchup")
                || name.contains("oil")
                || name.contains("mustard")
                || name.contains("dressing"))
            return true;
        else
            return false;
    }
}
