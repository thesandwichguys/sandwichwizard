package com.sandwichwizard.sandwichguys.sandwichwizard;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Static helper methods to easily manipulate SharedPreferences within the app
 *
 * @author Ogden Greene
 */
public class PreferencesHelper {
    public static final String FAVORITE = "favorite";
    public static final String YUCK = "yuck";
    public static final String FIRST_TIME = "first timer";

    /**
     * Gets an array of sandwich names stored in favorites. Null if empty.
     *
     * @param context application context
     * @return String array of sandwich names from favorites. Null if empty.
     */
    public static String[] getFavorites(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String string;
        if ((string = prefs.getString(FAVORITE, null)) == null)
            return new String[0];
        return string.split(",");
    }

    /**
     * Adds the sandwich to the preferences file as either a favorite or a yuck. Returns false if
     * the sandwich is already there. If the sandwich was in the opposite category, it's removed
     * from the old and written to the new.
     *
     * @param name sandwich name
     * @param context application context
     * @param mode FAVORITE or YUCK
     * @return true if the sandwich was written, false otherwise
     */
    public static boolean addSandwich(String name, Context context, String mode) {
        SharedPreferences prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        String curr;
        String opposite;
        if (containsSandwich(name, context, mode))
            return false;
        if (mode.equals(FAVORITE))
            opposite = YUCK;
        else
            opposite = FAVORITE;
        if (containsSandwich(name, context, opposite))
            removeSandwich(name, context, opposite);
        curr = prefs.getString(mode, "");
        curr += name + ",";
        editor.putString(mode, curr);
        editor.apply();
        return true;
    }

    /**
     * Checks to see if the sandwich name specified lies in the preference string for the given mode
     *
     * @param name sandwich name
     * @param context application context
     * @param mode FAVORITE or YUCK
     * @return true if sandwich name is in the preference for the mode
     */
    public static boolean containsSandwich(String name, Context context, String mode) {
        SharedPreferences prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String string;
        if ((string = prefs.getString(mode, null)) == null)
            return false;
        return string.contains(name + ",");
    }

    /**
     * Checks to see if they've used the app before
     *
     * @param context application context
     * @return false if they're new, true otherwise
     */
    public static boolean isFirstTime(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        return prefs.getBoolean(FIRST_TIME, true);
    }

    /**
     * Sets the preference for whether they've used the app before or not
     *
     * @param context application context
     * @param firstTime boolean
     */
    public static void setFirstTime(Context context, Boolean firstTime) {
        SharedPreferences prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(FIRST_TIME, firstTime);
        editor.apply();
    }

    /**
     * Gets a raw string of the preferences from the provided mode
     *
     * @param context application context
     * @param mode FAVORITE or YUCK
     * @return comma-separated and -terminated string of sandwiches in the preferences or null
     *         if empty
     */
    public static String getPrefs(Context context, String mode) {
        SharedPreferences prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        return prefs.getString(mode, null);
    }

    /**
     * Clears favorites and yucks and first-time check
     *
     * @param context application context
     */
    public static void resetAll(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(FAVORITE, null);
        editor.putString(YUCK, null);
        editor.putString(FIRST_TIME, null);
        editor.apply();
    }


    /**
     * Clears the given preference
     *
     * @param context application context
     * @param mode FAVORITE or YUCK
     */
    public static void reset(Context context, String mode) {
        SharedPreferences prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(mode, null);
        editor.apply();
    }

    /**
     * Removes the sandwich reference from the given preference. If not there, returns immediately
     *
     * @param name sandwich name
     * @param context application context
     * @param mode FAVORITE or YUCK
     */
    public static void removeSandwich(String name, Context context, String mode) {
        if (!containsSandwich(name, context, mode))
            return;
        SharedPreferences prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        String string = prefs.getString(mode, null);
        string = string.replace(name + ",", "");
        if (string.equals(""))
            editor.putString(mode, null);
        else
            editor.putString(mode, string);
        editor.apply();
    }
}
