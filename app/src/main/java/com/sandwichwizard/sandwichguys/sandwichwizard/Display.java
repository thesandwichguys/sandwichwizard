package com.sandwichwizard.sandwichguys.sandwichwizard;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Activity class for displaying a sandwich
 *
 * @author Ogden Greene, Luke Salamone
 */
public class Display extends Activity {
    SandwichMenu sandwichMenu;
    Sandwich sandwich;
    TextView text;
    ImageButton fave;
    ImageView image;
    // Set to true if a sandwich cannot be retrieved from the info given to us
    boolean error;
    Toast toast;
    // Sandwich ID
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        error = false;
        // Get sandwiches
        Intent i = getIntent();
        // Deserialize sandwiches in the new context if it changed, losing the old singleton
        if ((sandwichMenu = SandwichMenu.getSingleton()) == null) {
            SandwichMenu.deserializeSandwichMenu(this);
            sandwichMenu = SandwichMenu.getSingleton();
        }
        id = i.getStringExtra("id");
        // Check if error from no sandwich match close enough during quiz
        if (id.equals("0")) {
            CharSequence message = getResources().getText(R.string.no_sandwich);
            toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
            error = true;
        }
        // Retrieve sandwich sent by intent extra
        sandwich = sandwichMenu.getSandwichByID(id);
        // Check if error from having a nonzero ID that doesn't match (shouldn't happen ever)
        if (sandwich == null) {
            CharSequence message = getResources().getText(R.string.not_found);
            toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
            error = true;
        }
        text = (TextView) findViewById(R.id.sandwichTitle);
        image = (ImageView) findViewById(R.id.sandwichPicture);
        fave = (ImageButton) findViewById(R.id.buttonFave);
        // No error, set up widgets
        if (!error) {
            text.setText(sandwich.getName());
            image.setImageResource(sandwich.getPicture());
            if (PreferencesHelper.isFirstTime(this)) {
                CharSequence message = getResources().getString(R.string.tip1);
                toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
                toast.show();
                PreferencesHelper.setFirstTime(this, false);
            }
            if (PreferencesHelper.containsSandwich(sandwich.getName(), this, PreferencesHelper.FAVORITE))
                fave.setImageResource(R.drawable.favorited);

        }
        // Error display
        else {
            ((RelativeLayout) text.getParent()).setBackgroundColor(getResources().getColor(R.color.lightblue));
            text.setVisibility(View.INVISIBLE);
            image.setVisibility(View.INVISIBLE);
            toast.show();
        }
    }


    /**
     * Shows recipe by URL when sandwich imagebutton pressed
     *
     * @param view button view
     */
    public void recipe(View view) {
        Uri uri = Uri.parse(sandwich.getRecipeURL());
        startActivity(new Intent(Intent.ACTION_VIEW, uri));
    }

    /**
     * Returns to the home screen of the app
     *
     * @param view button view
     */
    public void returnHome(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * Adds or removes the sandwich from the favorites list and adjusts the button image accordingly
     *
     * @param view button view
     */
    public void fave(View view) {
        if(PreferencesHelper.containsSandwich(sandwich.getName(), this, PreferencesHelper.FAVORITE)) {
            PreferencesHelper.removeSandwich(sandwich.getName(), this, PreferencesHelper.FAVORITE);
            Toast.makeText(this, "Unfavorited!", Toast.LENGTH_SHORT).show();
            fave.setImageResource(R.drawable.favorite);
        }
        else {
            PreferencesHelper.addSandwich(sandwich.getName(), this, PreferencesHelper.FAVORITE);
            Toast.makeText(this, "Favorited!", Toast.LENGTH_SHORT).show();
            fave.setImageResource(R.drawable.favorited);
        }
    }

    /**
     * Adds the sandwich to the yucks list and removes from favorites if present
     *
     * @param view button view
     */
    public void yuck(View view) {
        if(PreferencesHelper.containsSandwich(sandwich.getName(), this, PreferencesHelper.YUCK))
            Toast.makeText(this, "Already yucked!", Toast.LENGTH_SHORT).show();
        else {
            if (PreferencesHelper.containsSandwich(sandwich.getName(), this, PreferencesHelper.FAVORITE))
                fave.setImageResource(R.drawable.favorite);
            PreferencesHelper.addSandwich(sandwich.getName(), this, PreferencesHelper.YUCK);
            Toast.makeText(this, "You won't see that one again!", Toast.LENGTH_SHORT).show();
            SandwichMenu.deserializeSandwichMenu(this);
        }
    }

    /**
     * Shares the sandwich as simple text
     *
     * @param view button view
     */
    public void share(View view){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "I have an irresistible " + sandwich.getName() + " craving right now!");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
